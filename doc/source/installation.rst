============
Installation
============

At the command line::

    $ pip install python-valenceclient

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv python-valenceclient
    $ pip install python-valenceclient
