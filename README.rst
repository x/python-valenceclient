===============================
python-valenceclient
===============================

Openstack client for Valence

Please fill here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/python-valenceclient
* Source: http://git.openstack.org/cgit/openstack/python-valenceclient
* Bugs: http://bugs.launchpad.net/python-valenceclient

Features
--------

* TODO
